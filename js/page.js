// JavaScript Document
$(document).ready(function(e) {
    var _jsWx = $(".jsWx"),
		_jsmeu = $(".jsmeu");
	_jsWx.hover(function(){
		$(this).children("img").stop().fadeIn(500);
		},function(){
			$(this).children("img").stop().fadeOut(500);
			})
	_jsmeu.hover(function(){
		$(this).children(".snav").stop().fadeIn(500);
		},function(){
			$(this).children(".snav").stop().fadeOut(500);
		})
	$(".snav").click(function(){
		$(this).hide();
		})
	if($(".indexFocus").length>0){
		var mySwiperFocus = new Swiper('.indexFocus', {
			pagination: '.swiper-pagination',
			paginationClickable: true,
			autoplay:2000,
			loop:true,
			calculateHeight:true,
			onSlideChangeStart:function(){
				mySwiperFocus.reInit();
			}
		}); 
		$('.prevImg').on('click', function(e){
			e.preventDefault()
			mySwiperFocus.swipePrev()
			})
		$('.nextImg').on('click', function(e){
			e.preventDefault()
			mySwiperFocus.swipeNext()
			})
		$(window).resize(function(){
			mySwiperFocus.reInit();
			})
		}else{
			return false
			}
});